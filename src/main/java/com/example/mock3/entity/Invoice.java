package com.example.mock3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Invoice {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long invoiceId;

	@Column(nullable = false)
	private String customerName;

	@Column(nullable = false)
	private String phone;

	@Column(nullable = false)
	private String address;
	
	@Column(nullable = false)
	private Double totalPrice;
	
	@Column(nullable = false)
	private Integer status;
	
	@OneToMany(mappedBy = "invoice", orphanRemoval = true)
	private List<InvoiceItem> invoiceItems;
	
}
