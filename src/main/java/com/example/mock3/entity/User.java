package com.example.mock3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	
	@Column(nullable = false, unique = true)
	private String username;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = true)
	private String fullname;
	
	@Column(nullable = true, unique = true)
	private String phone;

	@Column(nullable = true)
	private String address;
	
	@Column(nullable = false, unique = true)
	private String email;
	
	@Column(nullable = true)
	private String otpcode;

	@Column(nullable = true)
	private Boolean status;
	
	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	
	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private List<Review> reviews;
	
	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private List<Cart> carts;

	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}
}
