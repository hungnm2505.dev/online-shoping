package com.example.mock3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long categoryId;
	
	@Column(nullable = false, unique = true)
	private String categoryName;
	
	@Column(nullable = false)
	private Boolean status;
	
	@OneToMany(mappedBy = "category", orphanRemoval = true)
	private List<Product> products;
	
}
