package com.example.mock3.controllers;

import com.example.mock3.entity.User;
import com.example.mock3.security.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/manage")
public class AdminController {
    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/user/getall")
        @PreAuthorize("hasRole('ADMIN')")
        public String getall(){
            return userService.getall();
        }

    @DeleteMapping("/user/deleteByID/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteByID(@PathVariable Long userId){
        return userService.deleteUserByUserID(Long.valueOf(userId));
    }
    }


