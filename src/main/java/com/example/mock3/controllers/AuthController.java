package com.example.mock3.controllers;


import com.example.mock3.entity.EnumRole;
import com.example.mock3.entity.Role;
import com.example.mock3.entity.User;
import com.example.mock3.payload.request.LoginRequest;
import com.example.mock3.payload.request.SignupRequest;
import com.example.mock3.payload.request.VerifyEmailRequest;
import com.example.mock3.payload.response.MessageResponse;
import com.example.mock3.repository.RoleRepository;
import com.example.mock3.repository.UserRepository;
import com.example.mock3.security.jwt.JwtUtils;
import com.example.mock3.security.services.AccountService;
import com.example.mock3.security.services.JavaMailDemo;
import com.example.mock3.security.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

 private final UserService userService;
 private final AccountService accountService;

  public AuthController(UserService userService, AccountService accountService) {
    this.userService = userService;
      this.accountService = accountService;
  }

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
      return accountService.authenticateUser(loginRequest);
  }

  @PostMapping("/verify-email")
  public ResponseEntity<?> verifyEmail(@Valid @RequestBody VerifyEmailRequest verifyEmailRequest) {
      return accountService.verifyEmail(verifyEmailRequest);
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
      return accountService.registerUser(signUpRequest);
  }
}
