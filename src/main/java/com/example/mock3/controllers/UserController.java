package com.example.mock3.controllers;

import com.example.mock3.security.services.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class UserController {

private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/findByID/{id}")
    public String findById(@PathVariable String id){
        return userService.getOTPByUsername(id);
    }

    @GetMapping("/getall")
    public String getall(){
        return userService.currentAccount().getUserId().toString();
    }
}
