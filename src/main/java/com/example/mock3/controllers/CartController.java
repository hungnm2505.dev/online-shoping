package com.example.mock3.controllers;

import com.example.mock3.payload.response.CartDTO;
import com.example.mock3.payload.response.DataResponse;
import com.example.mock3.services.CartServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/shopping/cart")
public class CartController {

    private final CartServiceImpl cartService;

    public CartController(CartServiceImpl cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/getCart")
    public ResponseEntity getCart() {
        return ResponseEntity.ok(new DataResponse(1, cartService.getCartFromUser()));
    }

    @PostMapping("/addToCart")
    public ResponseEntity addToCart(@RequestBody Optional<CartDTO> item) {
        return ResponseEntity.ok(new DataResponse(1, cartService.insertItemToCart(item)));
    }

    @DeleteMapping("/remove/{id}")
    public ResponseEntity removeItemFromCart(@PathVariable Long id) {
        return ResponseEntity.ok(new DataResponse(1, cartService.removeItemFromCart(id)));
    }

    @DeleteMapping("/removeAll")
    public ResponseEntity removeAllItemsFromCart() {
        return ResponseEntity.ok(new DataResponse(1, cartService.removeAllItemsFromCart()));
    }

}
