package com.example.mock3.controllers;

import com.example.mock3.payload.response.DataResponse;
import com.example.mock3.payload.response.ProductDTO;
import com.example.mock3.services.ProductServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/shopping/product")
public class ProductController {

    private ProductServiceImpl service;

    public ProductController(ProductServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/get-all-products/{offset}/{limit}")
    public ResponseEntity showAll(@PathVariable int offset, @PathVariable int limit) {
        return ResponseEntity.ok(new DataResponse(1, service.selectAll(offset, limit)));
    }

    @PostMapping("/add-product")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity addProduct(@RequestBody Optional<ProductDTO> product) {
//        ResponseEntity.ok(new DataResponse(1, service.insert(product)));
        return service.insert(product);
    }

    @PutMapping("/edit-product/{id}")
    public ResponseEntity editProduct(@PathVariable Long id, @RequestBody Optional<ProductDTO> product) {
        return service.update(id, product);
    }

    @DeleteMapping("delete-product/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        return service.delete(id);
    }

}
