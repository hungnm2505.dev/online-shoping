package com.example.mock3.services;

import com.example.mock3.payload.response.CartDTO;

import java.util.List;
import java.util.Optional;

public interface CartService {

    List<CartDTO> getCartFromUser();

    CartDTO insertItemToCart(Optional<CartDTO> item);

    String removeItemFromCart(Long id);

    String removeAllItemsFromCart();

}
