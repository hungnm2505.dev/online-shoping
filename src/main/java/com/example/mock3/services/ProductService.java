package com.example.mock3.services;

import com.example.mock3.payload.response.ProductDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<ProductDTO> selectAll(int offset, int limit);

    ResponseEntity<?> insert(Optional<ProductDTO> product);

    ResponseEntity<?> update(Long id, Optional<ProductDTO> product);

    ResponseEntity<?> delete(Long id);

}
