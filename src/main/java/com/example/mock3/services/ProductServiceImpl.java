package com.example.mock3.services;

import com.example.mock3.entity.Category;
import com.example.mock3.entity.Product;
import com.example.mock3.payload.response.DataResponse;
import com.example.mock3.payload.response.MessageResponse;
import com.example.mock3.payload.response.ProductDTO;
import com.example.mock3.repository.CategoryRepository;
import com.example.mock3.repository.ImageRepository;
import com.example.mock3.repository.ProductRepository;
import com.example.mock3.repository.ReviewRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private ModelMapper modelMapper;
    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;
    private ImageRepository imageRepository;
    private ReviewRepository reviewRepository;

    public ProductServiceImpl(ModelMapper modelMapper, ProductRepository productRepository, CategoryRepository categoryRepository, ImageRepository imageRepository, ReviewRepository reviewRepository) {
        this.modelMapper = modelMapper;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<ProductDTO> selectAll(int offset, int limit) {
        return productRepository.findAll().stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<?> insert(Optional<ProductDTO> product) {
        if (product.isPresent()) {
            Product productToAdd = modelMapper.map(product.get(), Product.class);
            setCategoryByCategoryName(product, productToAdd);
            productRepository.save(productToAdd);
            return ResponseEntity.ok(new DataResponse(1, product));
        }
        return ResponseEntity.badRequest().body(new DataResponse(0, "Product to insert cannot be null"));
    }

    private void setCategoryByCategoryName(Optional<ProductDTO> product, Product productToAdd) {
        Category category = categoryRepository.findByCategoryNameIs(product.get().getCategoryCategoryName());
        if (category == null) {
            Category categoryToAdd = new Category();
            categoryToAdd.setCategoryName(product.get().getCategoryCategoryName());
            categoryToAdd.setStatus(true);
            categoryRepository.save(categoryToAdd);
            productToAdd.setCategory(categoryToAdd);
        } else {
            productToAdd.setCategory(category);
        }
    }

    @Override
    public ResponseEntity<?> update(Long id, Optional<ProductDTO> product) {
        if (product.isPresent()) {
            Product productToUpdate = productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product to update does not exist"));
            if (productToUpdate != null) {
                modelMapper.map(product.get(), productToUpdate);
                setCategoryByCategoryName(product, productToUpdate);
                productRepository.save(productToUpdate);
                return ResponseEntity.ok(new DataResponse(1, product));
            }
            return ResponseEntity.badRequest().body(new DataResponse(0, "Product to update does not exist"));
        }
        return ResponseEntity.badRequest().body(new DataResponse(0, "Product to update cannot be null"));
    }

    @Override
    public ResponseEntity<?> delete(Long id) {
        if (!productRepository.findById(id).isPresent()) return ResponseEntity.badRequest().body(new DataResponse(0, "Product to delete does not exist"));
        productRepository.deleteById(id);
        return ResponseEntity.ok(new DataResponse(1, "Delete successfully"));
    }
}
