package com.example.mock3.services;

import com.example.mock3.entity.Cart;
import com.example.mock3.entity.Product;
import com.example.mock3.entity.User;
import com.example.mock3.payload.response.CartDTO;
import com.example.mock3.repository.CartRepository;
import com.example.mock3.repository.ProductRepository;
import com.example.mock3.repository.UserRepository;
import com.example.mock3.security.services.UserImpl;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final UserImpl userService;
    private final ModelMapper modelMapper;

    public CartServiceImpl(CartRepository cartRepository, ProductRepository productRepository, UserRepository userRepository, UserImpl userService, ModelMapper modelMapper) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CartDTO> getCartFromUser() {
        return cartRepository.findByUser_UserId(userService.currentUser().getUserId()).stream().map(cart -> modelMapper.map(cart, CartDTO.class)).collect(Collectors.toList());
    }

    @Override
    public CartDTO insertItemToCart(Optional<CartDTO> item) {
        if (item.isPresent()) {
            Product product = productRepository.findById(item.get().getProductId())
                    .orElseThrow(() -> new EntityNotFoundException("Product does not exist!"));
            User user = userRepository.findById(userService.currentUser().getUserId())
                    .orElseThrow(() -> new EntityNotFoundException("User does not exist!"));
            Cart cartItem = cartRepository.findByUser_UserIdAndProduct_ProductId(user.getUserId(), product.getProductId());
            if (cartItem != null) {
                cartItem.setQuantity(cartItem.getQuantity() + item.get().getQuantity());
                cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
                cartRepository.save(cartItem);
                modelMapper.map(cartItem, item.get());
            } else {
                Cart cartItemToAdd = modelMapper.map(item.get(), Cart.class);
                cartItemToAdd.setTotalPrice(product.getPrice() * item.get().getQuantity());
                cartItemToAdd.setUser(user);
                cartRepository.save(cartItemToAdd);
                modelMapper.map(cartItemToAdd, item.get());
            }
            return item.get();
        }
        return null;
    }

    @Override
    public String removeItemFromCart(Long id) {
        Cart cartToRemove = cartRepository.findByUser_UserIdAndProduct_ProductId(userService.currentUser().getUserId(), id);
        if(cartToRemove != null) {
            cartRepository.delete(cartToRemove);
            return "Remove successfully";
        } else {
            return "Remove failed";
        }
    }

    @Override
    public String removeAllItemsFromCart() {
        List<Cart> cartList = cartRepository.findByUser_UserId(userService.currentUser().getUserId());
        cartList.stream().forEach(cartItem -> cartRepository.delete(cartItem));
        cartList.clear();
        return "Remove successfully";
    }
}
