package com.example.mock3.security.services;

import com.example.mock3.payload.request.LoginRequest;
import com.example.mock3.payload.request.SignupRequest;
import com.example.mock3.payload.request.VerifyEmailRequest;
import org.springframework.http.ResponseEntity;



public interface AccountService {
    ResponseEntity<?> authenticateUser(LoginRequest loginRequest);
    ResponseEntity<?> registerUser(SignupRequest signUpRequest);
     ResponseEntity<?> verifyEmail(VerifyEmailRequest verifyEmailRequest);
}
