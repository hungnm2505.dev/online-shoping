package com.example.mock3.security.services;


import com.example.mock3.entity.User;
import com.example.mock3.payload.request.LoginRequest;
import com.example.mock3.payload.response.DataResponse;
import com.example.mock3.payload.response.JwtResponse;
import com.example.mock3.payload.response.MessageResponse;
import com.example.mock3.repository.RoleRepository;
import com.example.mock3.repository.UserRepository;
import com.example.mock3.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserImpl implements UserService {

    private final UserRepository userRepository;

    public UserImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User currentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(authentication.getName()).orElseThrow(() -> new UsernameNotFoundException("This user does not exist!"));
        return user;
    }

    @Override
    public String getEmailByUsername(String username) {
        List<User> listuser = userRepository.findAll();
        String email = "not found";
        for (User u : listuser) {
            if (u.getUsername().equals(username)) {
                email = u.getEmail();
            }
        }
        return email;
    }

    @Override
    public Boolean getStatusByUsername(String username) {
        List<User> listuser = userRepository.findAll();
        Boolean status = false;
        for (User u : listuser) {
            if (u.getUsername().equals(username)) {
                status = u.getStatus();
            }
        }
        return status.booleanValue();
    }

    @Override
    public String getOTPByUsername(String username) {
        List<User> listuser = userRepository.findAll();
        String otpCode = "not found";
        for (User u : listuser) {
            if (u.getUsername().equals(username)) {
                otpCode = u.getOtpcode();
            }
        }
        return otpCode;
    }

    @Override
    public void updateStatusByname(String username) {
        List<User> listuser = userRepository.findAll();
        for (User u : listuser) {
            if (u.getUsername().equals(username)) {
                u.setStatus(true);
                userRepository.save(u);
            }
        }
    }

    @Override
    public ResponseEntity<?> deleteUserByUserID(Long userId) {
        List<User> listuser = userRepository.findAll();
        for (User u : listuser) {
            if (u.getUserId().equals(userId)) {
                userRepository.deleteById(userId);
            }
        }
       return ResponseEntity.ok(new MessageResponse("Delete success!"));
    }

    public String getall() {
        List<User> listUser = userRepository.findAll();
        return  listUser.toString();
    }
    public User currentAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User account = userRepository.findByUsername(authentication.getName()).orElseThrow(() -> new UsernameNotFoundException("Không tìm thấy tài khoản này!"));
        return account;
    }



}
