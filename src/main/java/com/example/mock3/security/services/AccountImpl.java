package com.example.mock3.security.services;

import com.example.mock3.entity.EnumRole;
import com.example.mock3.entity.Role;
import com.example.mock3.entity.User;
import com.example.mock3.payload.request.LoginRequest;
import com.example.mock3.payload.request.SignupRequest;
import com.example.mock3.payload.request.VerifyEmailRequest;
import com.example.mock3.payload.response.JwtResponse;
import com.example.mock3.payload.response.MessageResponse;
import com.example.mock3.repository.RoleRepository;
import com.example.mock3.repository.UserRepository;
import com.example.mock3.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AccountImpl implements AccountService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    private final UserService userService;

    public AccountImpl(UserService userService) {
        this.userService = userService;
    }


    @Override
    public ResponseEntity<?> authenticateUser(LoginRequest loginRequest) {

        if (userService.getStatusByUsername(loginRequest.getUsername()) == true) {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());

            return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
        } else return ResponseEntity.ok(new MessageResponse("Please active your account first!"));
    }

    @Override
    public ResponseEntity<?> registerUser(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(EnumRole.ROLE_CUSTOMER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(EnumRole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(EnumRole.ROLE_CUSTOMER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setStatus(false);
        JavaMailDemo javaMailDemo = new JavaMailDemo();
        String otpCode = javaMailDemo.generateOTP();
        user.setOtpcode(otpCode);
        javaMailDemo.sentEmail(signUpRequest.getEmail(), "Mã Xác Thực", otpCode);
        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!, please check your email to get the validation code"));
    }

    @Override
    public ResponseEntity<?> verifyEmail(VerifyEmailRequest verifyEmailRequest) {
        String OTP = userService.getOTPByUsername(verifyEmailRequest.getUsername());
        if (OTP.equals(verifyEmailRequest.getOtpcode())) {
            userService.updateStatusByname(verifyEmailRequest.getUsername());
            return ResponseEntity.ok(new MessageResponse("Active complete"));
        } else {
            return ResponseEntity.ok(new MessageResponse("Wrong code!, please try again!"));
        }
    }
}
