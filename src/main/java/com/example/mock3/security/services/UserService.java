package com.example.mock3.security.services;

import com.example.mock3.entity.User;
import com.example.mock3.payload.request.LoginRequest;
import org.springframework.http.ResponseEntity;

public interface UserService {
    String getEmailByUsername(String username);
    Boolean getStatusByUsername(String username);
    String getall();
    String getOTPByUsername(String username);
    void updateStatusByname(String username);
    ResponseEntity<?> deleteUserByUserID(Long userId);
    User currentAccount();

}
