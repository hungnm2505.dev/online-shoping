package com.example.mock3.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class VerifyEmailRequest {


        @NotBlank
        private String username;

        @NotBlank
        private String otpcode;

    }

