package com.example.mock3.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO implements Serializable {
    private String productName;
    private String description;
    private Double price;
    private Integer quantity;
    private Boolean status;
    private String categoryCategoryName;
    private List<ReviewDto> reviews;
    private List<String> imageImageNames;

    @Data
    public static class ReviewDto implements Serializable {
        private Integer star;
        private String content;
        private String userUsername;
    }
}
