package com.example.mock3.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO implements Serializable {
    private Integer quantity;
    private Double totalPrice;
    private Long userId;
    private Long productId;
}
