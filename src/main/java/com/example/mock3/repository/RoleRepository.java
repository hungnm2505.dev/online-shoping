package com.example.mock3.repository;


import com.example.mock3.entity.EnumRole;
import com.example.mock3.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  @Query("select r from Role r where r.roleName = ?1")
  Optional<Role> findByName(EnumRole name);
}
