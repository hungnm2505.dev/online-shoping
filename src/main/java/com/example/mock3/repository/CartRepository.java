package com.example.mock3.repository;

import com.example.mock3.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    List<Cart> findByUser_UserId(Long userId);

    @Query("select c from Cart c where c.product.productId = ?1")
    Cart findByProduct_ProductId(Long productId);

    @Query("select c from Cart c where c.user.userId = ?1 and c.product.productId = ?2")
    Cart findByUser_UserIdAndProduct_ProductId (Long userId, Long productId);

}